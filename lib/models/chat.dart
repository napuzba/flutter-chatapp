import 'package:chatapp/models/message.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class Chat {
  Chat(String title, String icon ) {
    this.title = title;
    this.icon  = icon;
  }
  String title = "";
  String icon = "";
  List<Message> messages = <Message>[];

  Message addMessage(String body) {
    Message message = Message(body, DateTime.now() );
    messages.add(message);
    return message;
  }
  Message addMyMessage(String body) {
    Message message = Message(body, DateTime.now() );
    messages.add(message);
    message.mine = true;
    return message;
  }

}