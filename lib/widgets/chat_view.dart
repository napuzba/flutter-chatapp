import 'package:chatapp/models/message.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import '../models/chat.dart';

class ChatViewWidget extends StatefulWidget {
  ChatViewWidget(Chat chat) : chat = chat {}
  final Chat chat;

  @override
  State<StatefulWidget> createState() {
    return ChatViewState(chat);
  }
}

class ChatViewState extends State<ChatViewWidget> {
  ChatViewState(Chat chat) : chat = chat {}

  Chat chat;

  ImageProvider<dynamic> _fetchImage() {
    ;
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Column(children: <Widget>[
          Expanded(
              child: Image(
                  fit: BoxFit.fill, image: AssetImage("assets/chat-back.png")))
        ]),
        Column(children: <Widget>[
          Expanded(
              child: ListView(
                  children: chat.messages.map((Message message) {
            return formatMessage(message);
          }).toList())),
          Container(
              margin: EdgeInsets.all(10),
              child: TextField(
                onSubmitted: (text) {
                  chat.addMyMessage(text);
                  setState(() {});
                },
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Message',
                ),
              ))
        ]),
      ],
    );
  }

  Widget formatMessage(Message message) {
    List<Widget> msg = <Widget>[];

    if (!message.mine) {
      msg.add(Flexible(
        flex: 2,
        child: Container(
          padding: EdgeInsets.all(20),
          color: Color.fromARGB(0xff, 0xff, 0xff, 0xff),
          child: Text(
            message.body,
            style: TextStyle(fontSize: 18),
          ),
        ),
      ));
      msg.add(Expanded(
        child: Container(
          color: Colors.blue,
          child: Container(),
        ),
      ));
    }
    if (message.mine) {
      msg.add(Expanded(
          child: Container(
        color: Colors.blue,
        child: Container(),
      )));
      msg.add(Flexible(
        flex: 2,
        child: Container(
          padding: EdgeInsets.all(20),
          color: Color.fromARGB(0xff, 0xDC, 0xF8, 0xC6),
          child: Text(
            message.body,
            style: TextStyle(fontSize: 18),
          ),
        ),
      ));
    }
    return Container(
        margin: EdgeInsets.only(top: 5, bottom: 5, left: 20,right: 20),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween, children: msg));
  }
}
