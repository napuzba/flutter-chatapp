import 'package:chatapp/routes/chat_view_route.dart';
import 'package:flutter/material.dart';
import 'package:fid_widgets/fid_widgets.dart';
import '../models/chat.dart';
import '../models/chat_list.dart';


class ChatListWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ChatListState();
  }
}

class ChatListState extends State<ChatListWidget> {
  ChatListState() {
    load();
  }

  ChatList chats = new ChatList();

  load() {
    var icon = 'https://cdn.vox-cdn.com/thumbor/Iy012aBHCHAESjEyc2KZWhweHaI=/0x0:4635x3090/1200x800/filters:focal(1928x400:2668x1140)/cdn.vox-cdn.com/uploads/chorus_image/image/66114445/1199341672.jpg.0.jpg';
    for ( int xx=0; xx < 10; xx++ ) {
      var chat = chats.add("Chat #"+ xx.toString(), icon);
      for ( int zz=0; zz < xx; zz++ ) {
        var msg = chat.addMessage("This is a message ${zz}\nThis is a message\nThis is a message\nThis is a message\n");
        msg.mine = (zz % 2 == 0);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
        children: chats.list.map( (Chat chat) {
           return Container(
              color: Color.fromARGB(0xff, 0xff, 0xff, 0xff),
              padding: EdgeInsets.only(right:10,left:13,top:10,bottom: 13),
              child: Row(
                children: <Widget>[
                  Container (
                    child: ShapedImage(
                      imagePath: chat.icon,
                      imageSource: ImageSourceType.Network,
                      shape: ImageShapeType.Circle,
                      raduis: 25,
                    ),
                    margin: EdgeInsets.only(right: 13),
                  ),
                  Expanded (
                    child: FlatButton(
                      onPressed: () {
                        print("Chat clicked");
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => ChatViewRoute(chat)),
                        );
                      },
                      child: Container (
                        child : Column (
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Flexible (
                                  flex: 1,
                                  child:Text(chat.title,
                                    style: TextStyle(fontSize: 20, color: Color.fromARGB(0xff, 0x22, 0x22, 0x22), fontWeight: FontWeight.w900 ),
                                  )
                                ),
                                Text("9:03",
                                  style: TextStyle(fontSize: 14, color: Color.fromARGB(0xff, 0x09, 0xD2, 0x61) ),
                                )
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text("hallo",
                                  style: TextStyle(fontSize: 20, color: Color.fromARGB(0xff, 0x75, 0x75, 0x75)),
                                )
                              ],
                            )
                          ]
                        )
                      )
                    )
                  )
                ]
              )
           );
        }).toList()
    );
  }
}
