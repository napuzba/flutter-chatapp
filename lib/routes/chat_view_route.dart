import 'package:chatapp/widgets/chat_view.dart';
import 'package:flutter/material.dart';
import 'package:fid_widgets/fid_widgets.dart';
import 'theme.dart';
import '../models/chat.dart';

enum MenuChat {
  item1,
  item2,
}

class ChatViewRoute extends StatelessWidget {
  ChatViewRoute(Chat chat) {
    this.chat = chat;
  }

  Chat chat;

  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar: buildAppBar(), body: ChatViewWidget(chat));
  }

  Widget buildAppBar() {
    var appbar = PreferredSize(
        preferredSize: Size.fromHeight(50),
        child: SizedBox(
            child: AppBar(
                backgroundColor: theme.appbar_color,
                actions: <Widget>[
                  IconButton(
                    icon: Icon(Icons.camera),
                    onPressed: () {},
                  ),
                  IconButton(
                    icon: Icon(Icons.chat),
                    onPressed: () {},
                  ),
                  buildPopupMenu()
                ],
                title: Container(
                  child: Row(
                    children: <Widget>[
                      Container(
                          padding: EdgeInsets.only(right: 10),
                          child: ShapedImage(
                        imagePath: chat.icon,
                        imageSource: ImageSourceType.Network,
                        shape: ImageShapeType.Circle,
                        raduis: 20,
                      )),
                      Expanded(
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: <Widget>[
                            Text(chat.title,
                                style: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.w900)),
                            Text(chat.title,
                                style: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.w200)),
                          ]))
                    ],
                  ),
                ))));
    return appbar;
  }

  PopupMenuButton<MenuChat> buildPopupMenu() {
    return PopupMenuButton<MenuChat>(
      onSelected: (MenuChat result) {
        print(result);
      },
      itemBuilder: (BuildContext context) => <PopupMenuEntry<MenuChat>>[
        const PopupMenuItem<MenuChat>(
          value: MenuChat.item1,
          child: Text('Menu1-Item1'),
        ),
        const PopupMenuItem<MenuChat>(
          value: MenuChat.item2,
          child: Text('Menu1-Item2'),
        )
      ],
    );
  }
}
