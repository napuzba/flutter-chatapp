import 'package:chatapp/widgets/chat_list.dart';
import 'package:flutter/material.dart';
import 'theme.dart';

enum MenuList {
  item1,
  item2,
}

class ChatListRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: theme.appbar_color,
          title: Text('My Chat App'),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.search),
              onPressed: () {},
            ),
          ],),
        body: ChatListWidget()
    );
  }
}